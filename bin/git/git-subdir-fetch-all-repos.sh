source ~/.fah/bash/colours.sh
CUR=$(echo $PWD)
for dir in $(find ${CUR} -name ".git"); do
	DIR=$(dirname $dir)
	FULLDIR=$CUR/${DIR}
	FULLDIR=${DIR}
	cd $FULLDIR
	printf "${TXTBoldCyan}${FULLDIR}${TXTNoColour}\\n"

	OUTPUT=$(git fetch --all --prune)
	IFS=$'\n'
	read -r -a array <<<"${OUTPUT}"
	for i in "${array[@]}"; do
		IFS=$'\r'
		read -r -a array_cr <<<"${OUTPUT}"
		for remote_name in "${array_cr[@]}"; do
			printf "${TXTNoColour} [${TXTYellow}${FULLDIR}${TXTNoColour}] $remote_name\\n"
		done
	done
done
