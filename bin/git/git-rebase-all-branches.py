#!/usr/bin/env python3
import argparse
import os
import subprocess
import sys
import typing

TERMINAL_WIDTH = 86


def _get_current_branch_name() -> typing.Optional[str]:
    """Returns the current Git branch name or None if in detached HEAD state or during a
    rebase.

    Returns:
        Optional[str]: The current Git branch name or None if it cannot be determined.
    """
    try:
        branch_name = (
            subprocess.check_output(
                ["git", "branch", "--show-current"], stderr=subprocess.DEVNULL
            )
            .strip()
            .decode()
        )
        if not branch_name:  # This checks for detached HEAD state
            return None
        return branch_name
    except subprocess.CalledProcessError as err:
        print(err)
        # Handle cases where the git command fails, e.g., not a git repository
        # or in the midst of a rebase
        return None


def _print_label(msg: str) -> None:
    print("fah: " + "-" * TERMINAL_WIDTH)
    print(f"fah: {msg}")
    print("fah: " + "-" * TERMINAL_WIDTH)


def _run_command(
    label: str,
    verbose: bool,
    cmd: typing.List[str],
    env_overrides: typing.Optional[dict] = None,
) -> typing.Optional[typing.List[str]]:
    """Runs the given command and throws on any error."""
    # create a new env with overrides
    run_env = None
    if env_overrides:
        # merge the overrides into the env
        run_env = {**os.environ.copy(), **env_overrides}

    if verbose:
        print(label)
        print(" ".join(cmd))

    try:
        if run_env:
            process = subprocess.run(
                cmd,
                check=True,
                env=run_env,
                capture_output=True,
            )  # raise on non-zero
        else:
            process = subprocess.run(
                cmd,
                check=False,
                capture_output=True,
            )  # raise on non-zero
        if 0 != process.returncode:
            raise RuntimeError(f"exit code was {process.returncode}")
    except BaseException as err:
        cmd_string: str = " ".join(cmd)
        stdout: str
        stderr: str
        try:
            stdout = (
                process.stdout.decode()
                .replace("\r", "\n")
                .replace("\n", "\nfah: stdout: ")
            )
            stderr = (
                process.stderr.decode()
                .replace("\r", "\n")
                .replace("\n", "\nfah: stderr: ")
            )
        except UnboundLocalError:
            stdout = "fah: No process started, does it exist?"
            stderr = ""
        error_string = (
            f"\nfah: FATAL: command failed: {label}"
            f"\n\t{cmd_string}"
            f"\nfah: stdout: {stdout}"
            f"\nfah: stderr: {stderr}"
        )
        raise RuntimeError(error_string) from err
    lines = process.stdout.decode("utf-8").split("\n")
    if verbose:
        print("\n".join(lines))

    return lines


def _ensure_state(base_branch: str, verbose: bool) -> None:
    _print_label("ENSURING SYSTEM AOK")

    try:
        _run_command("check for changes", verbose, "git diff-index HEAD --".split(" "))
    except RuntimeError as err:
        print("fah: You have local changes! Will not continue")
        print(str(err))
        sys.exit(2)

    try:
        _run_command(
            "initial checkout of branch",
            verbose,
            f"git checkout {base_branch}".split(" "),
        )
    except RuntimeError as err:
        print(f"fah: Filed to checkout '{base_branch}', do you have local changes?")
        print(err)
        sys.exit(2)


def _get_branches(verbose: bool) -> typing.List[str]:
    lines_out = _run_command(
        "getting list of branches",
        verbose,
        'git for-each-ref --shell --format="%(refname)" refs/heads/'.split(" "),
    )

    if lines_out is None:
        return []

    # jump through some std-out hoops
    # TODO: there has to be a better way
    branches = [
        b.replace(r"""b'"\'refs/heads/""", "")
        .replace("refs/heads/", "")
        .replace('"', "")
        .replace(r"\'", "")
        .replace("'", "")
        for b in lines_out
    ]
    branches = [b for b in branches if b]
    return branches


def _get_branch_upstream(branch: str, verbose: bool) -> typing.Optional[str]:
    try:
        std_out = _run_command(
            f"getting tracking branch for '{branch}'",
            verbose,
            cmd=[
                "git",
                "rev-parse",
                "--abbrev-ref",
                f"{branch}" "@{upstream}",
            ],
        )
    except RuntimeError:
        return None

    if std_out is None:
        return None

    tracking = std_out[0]
    print(f"fah:   tracks {tracking}")
    return tracking


def _checkout_branch(branch: str, verbose: bool) -> None:
    _run_command(
        f"checking out '{branch}'",
        verbose,
        cmd=[
            "git",
            "checkout",
            branch,
        ],
    )


def _rebase_against(
    branch: str,
    base_branch: str,
    skip_bad: bool,
    exec_cmd: typing.Optional[str],
    verbose: bool,
) -> None:
    cmd: typing.List[str] = [
        "git",
        "rebase",
        base_branch,
    ]
    if exec_cmd:
        cmd = cmd + ["--exec", exec_cmd]
    try:
        _run_command(f"rebasing '{branch}' against '{base_branch}'", verbose, cmd=cmd)
    except RuntimeError:
        if skip_bad:
            print(f"fah: FAILED: rebasing '{branch}' on '{base_branch}', skipping")
            _run_command(
                f"aborting rebase '{branch}' against '{base_branch}'",
                verbose,
                cmd=["git", "rebase", "--abort"],
            )
            return
        print(f"fah: FAILED: rebasing '{branch}' on '{base_branch}', skipping")
        raise  # if not skippoing re-raise
    # print(f"fah: PASS: rebasing '{branch}' on '{base_branch}'")


def _run_for_branch(
    branch: str,
    base_branch: str,
    skip_bad: bool,
    re_rebase_against_tracking: bool,
    exec_cmd: typing.Optional[str],
    verbose: bool,
) -> None:
    label = f"fah: rebasing {branch}"
    if verbose:
        _print_label(label)
    else:
        print(label)
    upstream = _get_branch_upstream(branch, verbose)
    _checkout_branch(branch, verbose)
    _rebase_against(branch, base_branch, skip_bad, exec_cmd, verbose)
    if upstream and re_rebase_against_tracking:
        _rebase_against(
            branch,
            upstream,
            skip_bad,
            exec_cmd,
            verbose,
        )


def _run_for_each_branch(
    branches: typing.List[str],
    base_branch: str,
    skip_bad: bool,
    re_rebase_against_tracking: bool,
    exec_cmd: typing.Optional[str],
    verbose: bool,
) -> None:
    for branch in branches:
        _run_for_branch(
            branch, base_branch, skip_bad, re_rebase_against_tracking, exec_cmd, verbose
        )


def _parse_args(argv: typing.List[str]) -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        description=("Dashboard in a box for the Lursight service")
    )
    parser.add_argument(
        "-b",
        "--base-branch",
        dest="base_branch",
        default="lursight/main",
        required=False,
    )
    parser.add_argument(
        "--exec",
        dest="exec_cmd",
        default=None,
        required=False,
    )
    parser.add_argument(
        "-s",
        "--skip-bad",
        dest="skip_bad",
        action=argparse.BooleanOptionalAction,
        default=False,
        required=False,
    )
    parser.add_argument(
        "-t",
        "--use-tracking",
        dest="re_rebase_against_tracking",
        action=argparse.BooleanOptionalAction,
        default=False,
        required=False,
    )
    parser.add_argument(
        "-v",
        "--verbose",
        dest="verbose",
        action=argparse.BooleanOptionalAction,
        default=False,
        required=False,
    )
    args = parser.parse_args(argv[1:])
    return args


def main(argv: typing.List[str]) -> None:
    original_branch_name: typing.Optional[str] = _get_current_branch_name()

    args = _parse_args(argv=argv)
    _ensure_state(args.base_branch, args.verbose)

    print(f"Current branch name: {original_branch_name}")

    branches = _get_branches(args.verbose)
    _run_for_each_branch(
        branches=branches,
        base_branch=args.base_branch,
        skip_bad=args.skip_bad,
        re_rebase_against_tracking=args.re_rebase_against_tracking,
        exec_cmd=args.exec_cmd,
        verbose=args.verbose,
    )
    _print_label(f"DONE!: All rebases complete, returning to '{args.base_branch}'")

    return_branch: str = args.base_branch
    if original_branch_name:
        return_branch = original_branch_name

    _checkout_branch(return_branch, args.verbose)


if __name__ == "__main__":
    main(sys.argv)
