

if ! git diff-index --quiet HEAD --
then
	printf "fah: You have local changes! Will not continue\\n"
	exit 2
fi

if [ -z "$REMOTE_SRC" ]; then
  REMOTE_SRC="origin"  # TODO: customise this
fi

REMOTE_BRANCHES=$(git branch -a | grep remotes/$REMOTE_SRC | grep -v "\->\|master" | sed -e 's#remotes/'$REMOTE_SRC'/##g')
echo $REMOTE_BRANCHES;
for BRANCH in $REMOTE_BRANCHES; do
  printf "fah: Attempting to create remote branch '%s'\\n" "$BRANCH"
  # try doing a soft checkout of the branch, meaning we
  # should create it and move on (no -b switch)
  if ! git checkout $BRANCH
  then
    printf "fah: ...failed to create branch '%s'\\n" "$BRANCH"
  else
    printf "fah: ...created branch '%s'\\n" "$BRANCH"
  fi
done

# now rebase all the created branches
git-rebase-all-branches.bash $@

