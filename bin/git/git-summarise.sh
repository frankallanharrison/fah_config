set -o pipefail
source ~/.fah/bash/colours.sh

function get_repo_branch_infos() {
	BRANCH_INFOS=$(git branch -vv | sed 's/[\* \t]//')
	if [[ $? != 0 ]]; then
		printf "get_repo_branch_infos failed\\n"
		exit 2
	fi
	echo "${BRANCH_INFOS}"
}

function git_branch_name() {
	git branch 2>/dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/'
	if [[ $? != 0 ]]; then
		printf "git_branch_name failed\\n"
		exit 2
	fi
}

function count_edited_files() {
	git status 2>&1 | grep "modified:\|modificado" | wc -l | tr -d -c 0-9
}

function count_remotes() {
	git remote -v 2>&1 | grep "fetch" | wc -l | tr -d -c 0-9
}

function count_dead_remotes() {
	git remote -v 2>&1 | grep "fetch" | grep "lonefish\|git@github.com\|git@bitbucket.org" | wc -l | tr -d -c 0-9
}

function upstream_candidate_branches() {
	CUR_BRANCH=${1}
	git branch -r | grep "${CUR_BRANCH}"
	if [[ $? != 0 ]]; then
		printf "upstream_candidate_branches failed\\n"
		exit 2
	fi
}

function get_branch_tag() {
	BRANCH=$1
	if [[ $BRANCH == *"["*":"*"]"* ]]; then
		if [[ $BRANCH == *"ahead"*"behind"* || $BRANCH == *"delante"*"detrás"* ]]; then
			printf "${TXTRed}DIVERGED${TXTNoColour}\\n"
		else
			if [[ $BRANCH == *"ahead"* || $BRANCH == *"delante"* ]]; then
				printf "${TXTRed}AHEAD${TXTNoColour}\\n"
			else
				if [[ $BRANCH == *"behind"* || $BRANCH == *"detrás"* ]]; then
					printf "${TXTGreen}BEHIND${TXTNoColour}\\n"
				else
					if [[ $BRANCH == *"gone"* ]]; then
						printf "${TXTRed}UPSTREAM DELETED${TXTNoColour}\\n"
					else
						printf "${TXTYellow}CHECK ME${TXTNoColour}\\n"
					fi
				fi
			fi
		fi
	else
		if [[ $BRANCH == *"["*"/"*"]"* || $BRANCH == *"HEAD detached"* || $BRANCH == *"HEAD desacoplada"* ]]; then
			printf "OK\\n"
		#else
		#	printf "${TXTRed}NO UPSTREM BRANCH${TXTNoColour} $(echo ${BRANCH} | awk '{print $1}')\\n"
		#	for CAND in $(upstream_candidate_branches);
		#	do
		#		printf "\t\t$CAND\\n"
		#	done
		else
			printf "${TXTRed}NO UPSTREAM!${TXTNoColour}\\n"
		fi
	fi
}

function annotate_branch_info() {
	BRANCH_INFO=$1
	TAG=$(get_branch_tag $BRANCH_INFO)

	if [[ $TAG != *"OK"* ]]; then
		printf "    %s: %s\\n" "$TAG" "${BRANCH_INFO:0:120}"
	fi
}

function get_repo_data() {
	DIR_NAME=$1
	cd "${DIR_NAME}"

	REPO_OK=$(git status 2>&1)
	EXIT_CODE=$?
	if [[ $EXIT_CODE != 0 ]]; then
		printf "\\n   ${TXTRed}REPOSITORY IS CORRUPT${TXTNoColour} (exit from git-statue was %s)\\n" "$EXIT_CODE"
		return
	fi

	CUR_BRANCH=$(git_branch_name)

	EDITED_FILES=$(count_edited_files)

	REMOTES=$(count_remotes)
	EXIT_CODE=$?
	if [[ $EXIT_CODE != 0 ]]; then
		printf "get_repo_data (3) failed with %s and %s\\n" "$EXIT_CODE" "$REMOTES"
		return
	fi

	DEAD_REMOTES=$(count_dead_remotes)

	if [[ ${REMOTES} == 0 ]]; then
		printf " ${TXTRed}${REMOTES} remotes${TXTNoColour}"
	else
		printf " ${REMOTES} remotes"
	fi

	if [[ ${DEAD_REMOTES} != 0 ]]; then
		printf ", ${TXTRed}${REMOTES} dead remotes${TXTNoColour}"
	fi

	if [[ ${EDITED_FILES} != 0 ]]; then
		printf "${TXTYellow}${CUR_BRANCH}${TXTNoColour}, ${TXTRed}has %s edits${TXTNoColour}" "${EDITED_FILES}"
	fi

	printf "\\n"

	BRANCH_INFOS="$(get_repo_branch_infos)"
	if [[ $? != 0 ]]; then
		printf "get_repo_data (2) failed\\n"
		exit 2
	fi
	IFS=$'\n'
	for BRANCH_INFO in ${BRANCH_INFOS}; do
		annotate_branch_info "$BRANCH_INFO"
	done
}

#get_repo_data "/Users/frank/Development/angular-google-gapi_example"
#exit 2

if [ -z $1 ]; then
	CUR="."
else
	CUR=$(echo $1)
fi
printf "Search from: $CUR\\n"
CD_PFX=${CUR}/
if [[ ${CUR:0:1} == "/" ]]; then
	CD_PFX=
fi
if [[ ${CD_PFX} == "./" ]]; then
	CD_PFX=
fi

find ${CUR} -name ".git" -type d -print0 |
	while IFS= read -r -d '' dir; do
		DIR=$(dirname "$dir")
		if [[ $? != 0 ]]; then
			printf "FUCKED 1\\n"
		fi
		printf "${TXTCyan}$DIR${TXTNoColour}: "
		CUR_DIR=$(pwd)
		get_repo_data "${CD_PFX}${DIR}"
		if [[ $? != 0 ]]; then
			printf "FUCKED\\n"
		fi
		cd $CUR_DIR
	done
