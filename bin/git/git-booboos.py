#!/usr/bin/env python3
# pylint: disable=invalid-name
"""Counts the number of FIXME (and related) comments in code as a measure of code
quality."""
import os
import time
from typing import Dict, List, Literal

from git import Repo, compat  # type: ignore

BoobooKey = Literal[
    "todo",
    "fixme",
    "xxx",
    "istanbul-ignore-next",
    "istanbul-ignore-if",
    "istanbul-ignore-else",
    "pragma:-TODOCOVME",
    "pragma:-no-cover",
]
BoobooCounts = Dict[BoobooKey, int]


def _process_counts(diff: List[str]) -> BoobooCounts:
    booboo_types: List[BoobooKey] = [
        "todo",
        "fixme",
        "xxx",
        "istanbul-ignore-next",
        "istanbul-ignore-if",
        "istanbul-ignore-else",
        "pragma:-TODOCOVME",
        "pragma:-no-cover",
    ]
    counts: BoobooCounts = {"todo": 0, "fixme": 0, "xxx": 0}
    for line in diff:
        if not line:
            continue
        for booboo in booboo_types:
            first_char = line[0]
            line.replace(":", " ").replace("#", " ").replace("//", " ").replace(
                "istanbul ignore ", "istanbul-ignore-"
            ).replace("pragma: TODOCOVME", "pragma:-TODOCOVME").replace(
                "pragma: no cover", "pragma:-no-cover"
            )
            tokens = line.lower().split()
            if booboo not in tokens:
                continue
            # print(tokens)
            if first_char == "+":
                counts[booboo] += 1
            elif first_char == "-":
                counts[booboo] -= 1
            elif first_char == " ":
                pass
    return counts


def _process_diff(diff: List[str]) -> BoobooCounts:
    return _process_counts(diff)


def analyse_per_commit(repo: Repo) -> None:
    """TODO: document me"""
    all_commits = reversed(list(repo.iter_commits("HEAD")))  # can't reverse generator

    print(
        ",".join(
            (
                "sha",
                "date",
                "todo",
                "fixme",
                "xxx",
                "istanbul-ignore-next",
                "istanbul-ignore-if",
                "istanbul-ignore-else",
                "pragma:-TODOCOVME",
                "pragma:-no-cover",
                "sum(todo)",
                "sum(fixme)",
                "sum(xxx)",
                "sum(istanbul-ignore-next)",
                "sum(istanbul-ignore-if)",
                "sum(istanbul-ignore-else)",
                "sum(pragma:-TODOCOVME)",
                "sum(pragma:-no-cover)",
            )
        )
    )
    running_count: BoobooCounts = {
        "todo": 0,
        "fixme": 0,
        "xxx": 0,
        "istanbul-ignore-next": 0,
        "istanbul-ignore-if": 0,
        "istanbul-ignore-else": 0,
        "pragma:-TODOCOVME": 0,
        "pragma:-no-cover": 0,
    }
    for commit in all_commits:
        if not commit.parents:
            # First commit, ignore (can't diff against prev)
            continue

        # Has a parent
        diff = commit.diff(commit.parents[0], create_patch=True, R=True)
        # data.commit.authored_date
        counts: BoobooCounts = {
            "todo": 0,
            "fixme": 0,
            "xxx": 0,
            "istanbul-ignore-next": 0,
            "istanbul-ignore-if": 0,
            "istanbul-ignore-else": 0,
            "pragma:-TODOCOVME": 0,
            "pragma:-no-cover": 0,
        }
        for key_name in diff:
            try:
                # Get the patch message
                msg = key_name.diff.decode(compat.defenc)
                lines: List[str] = msg.split("\n")
                counts = _process_diff(lines)
            except UnicodeDecodeError:
                continue
        if sum(counts.values()) != 0:
            google_sheets_date = time.strftime(
                "%d/%m/%Y", time.gmtime(commit.authored_date)
            )
            running_count["todo"] += counts["todo"]
            running_count["fixme"] += counts["fixme"]
            running_count["xxx"] += counts["xxx"]
            running_count["istanbul-ignore-next"] += counts["istanbul-ignore-next"]
            running_count["istanbul-ignore-if"] += counts["istanbul-ignore-if"]
            running_count["istanbul-ignore-else"] += counts["istanbul-ignore-else"]
            running_count["pragma:-TODOCOVME"] += counts["pragma:-TODOCOVME"]
            running_count["pragma:-no-cover"] += counts["pragma:-no-cover"]
            print(
                ",".join(
                    (
                        commit.hexsha,
                        f"{google_sheets_date}",
                        str(counts["todo"]),
                        str(counts["fixme"]),
                        str(counts["xxx"]),
                        str(counts["istanbul-ignore-next"]),
                        str(counts["istanbul-ignore-if"]),
                        str(counts["istanbul-ignore-else"]),
                        str(counts["pragma:-TODOCOVME"]),
                        str(counts["pragma:-no-cover"]),
                        str(running_count["todo"]),
                        str(running_count["fixme"]),
                        str(running_count["xxx"]),
                        str(running_count["istanbul-ignore-next"]),
                        str(running_count["istanbul-ignore-if"]),
                        str(running_count["istanbul-ignore-else"]),
                        str(running_count["pragma:-TODOCOVME"]),
                        str(running_count["pragma:-no-cover"]),
                    )
                )
            )

        # sys.exit(10)


def main():
    """Main entry point for count technical debt in a git repo."""
    repo_path = str(os.getcwd())
    print(f"loading git repo at {repo_path}")
    repo = Repo(repo_path)
    assert not repo.bare

    analyse_per_commit(repo=repo)

    # print(repo.commit("8f8bb1dc"))
    # columns = ["sha", "commiter", "date", "msg-line"]
    # data_frame = pandas.read_csv(
    #     filepath_or_buffer=("a.csv"), delimiter=";", header=None, names=columns
    # )
    # print(list(data_frame.iterrows())[0])
    # prev: Optional[str] = None
    # for row in data_frame.iterrows():
    #     sha = row[1]["sha"]
    #     if prev:
    #         print(repo.commit(sha))
    #         print(repo.dif)
    #         sys.exit(10)
    #     prev = sha


if __name__ == "__main__":
    main()
