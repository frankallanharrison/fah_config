#!/usr/bin/env python3
"""Accesses files in a directory, triggering an iCloud read."""
import argparse
import concurrent.futures
import sys
import time
from datetime import datetime
from pathlib import Path

import requests


def check_icloud_access(url: str = "https://www.apple.com") -> None:
    """Checks if there is internet access and specifically access to an Apple URL
    (indicative of iCloud access).

    :param url: The URL to test access against.
    """
    try:
        response = requests.get(url, timeout=5)
        response.raise_for_status()  # Raises an exception for 4XX/5XX errors
    except (requests.RequestException, requests.ConnectionError):
        print("No internet access or iCloud access is unavailable.")
    sys.exit(1)


def read_file(file_path: Path) -> None:
    """Attempt to read the file content."""
    # print(str(file_path))
    try:
        file_path.read_bytes()
        # print(f"aok reading '{str(file_path)}'")
    except FileNotFoundError as err:
        print(f"file has disappeared: '{str(file_path)}': {str(err)}")
    except Exception as err:
        print(f"error reading '{str(file_path)}': {str(err)}")
        raise


def ensure_local_copy(directory: Path) -> int:
    """Iterates over files in the specified directory, attempting to read them to
    trigger local storage by iCloud.

    :param directory: The directory path to check and ensure files are stored locally.
    """
    # Collect all file paths first to avoid race conditions in multithreading
    file_paths = [
        file_path for file_path in directory.glob("**/*") if file_path.is_file()
    ]
    num_files: int = len(list(file_paths))
    if num_files < 10:
        raise RuntimeError(f"No files found in '{str(directory)}'")
    timeout: int = 2
    # Use ThreadPoolExecutor to read files in parallel
    if True:  # pylint: disable=using-constant-test
        for file_path in file_paths:
            read_file(file_path)
    else:
        with concurrent.futures.ThreadPoolExecutor() as executor:
            futures = [
                executor.submit(read_file, file_path) for file_path in file_paths
            ]

            # Wait for all futures to complete, if needed
            try:
                for future in concurrent.futures.as_completed(futures):
                    try:
                        future.result(
                            timeout=timeout
                        )  # You can handle each result or catch exceptions here
                    except concurrent.futures.TimeoutError:
                        print("Timeout accessing")
            except KeyboardInterrupt:
                print()
                print("Interrupted by user, cancelling tasks...")
                for future in futures:
                    future.cancel()  # Attempt to cancel all submitted futures
                print("All tasks cancelled.")
    return num_files


def main() -> None:
    """Main function to parse command line arguments and call the file reading
    function."""
    parser = argparse.ArgumentParser(description="Ensure local copies of iCloud files.")
    parser.add_argument(
        "directory", type=Path, help="Directory path to ensure local copy of files."
    )
    args = parser.parse_args()

    while True:
        print("Starting")
        time_start: float = time.time()
        number_files: int = ensure_local_copy(args.directory)
        time_taken: float = time.time() - time_start
        timestamp_str: str = datetime.now().strftime("%Y-%m-%d-%H:%M")
        print(f"Done aok: {timestamp_str}: {number_files} files: took {time_taken}")
        sleep_time_s: int = 300
        print(f"sleeping for {sleep_time_s}s")
        time.sleep(sleep_time_s)


if __name__ == "__main__":
    main()
