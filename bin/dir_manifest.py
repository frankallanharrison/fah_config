#!/usr/bin/env python3
"""A tool for generating a manifest of a directory, recursively.

Attempts to get CRCs for each file and directory so duplicates can be detected.
"""

import argparse
import csv
import mmap
import sys
import time
import zlib
from datetime import datetime
from pathlib import Path
from typing import Dict, Tuple


def human_readable_size(size_in_bytes: int) -> str:
    """Convert a size in bytes to a human-readable format (e.g., KB, MB, GB)."""
    units = ["B", "KB", "MB", "GB", "TB", "PB"]
    size: float = float(size_in_bytes)
    unit_index = 0

    while size >= 1024 and unit_index < len(units) - 1:
        size /= float(1024)
        unit_index += 1

    return f"{size:.2f} {units[unit_index]}"


def calculate_crc32(file_path: Path) -> str:
    """Calculate the CRC32 checksum of a file."""
    crc32_hash = 0
    with open(file_path, "rb") as f:
        # Memory-map the file for faster access
        with mmap.mmap(f.fileno(), 0, access=mmap.ACCESS_READ) as mm:
            # Use memory directly instead of reading in blocks
            crc32_hash = zlib.crc32(mm)
    return f"{crc32_hash & 0xFFFFFFFF:08x}"


def get_modified_time(path: Path) -> str:
    """Return the last modified date in ISO 8601 format."""
    return datetime.fromtimestamp(path.stat().st_mtime).isoformat()


def process_directory(
    directory: Path,
    file_info: Dict[Path, Tuple[int, float, str]],
    dir_info: Dict[Path, Tuple[int, float, str]],
    csv_writer: csv.writer,
    last_print_dot: bool,
) -> bool:
    """Recursively process files and directories in a leaf-first manner.

    Populates file_info with file metadata and dir_info with directory metadata.
    """
    dir_size = 0
    dir_crc = 0
    start_time_dir = time.time()

    # Process all files and directories in this directory
    for entry in sorted(directory.iterdir(), key=lambda p: p.name):
        try:
            if entry.is_file():
                # Get file size and CRC32, store in the file_info lookup
                start_time_file = time.time()
                file_size = entry.stat().st_size
                file_crc = calculate_crc32(entry)
                time_taken = time.time() - start_time_file
                file_info[entry] = (file_size, time_taken, file_crc)

                # Update current directory's CRC and size
                dir_size += file_size
                dir_crc = zlib.crc32(file_crc.encode("utf-8"), dir_crc)

                if time_taken > 1:
                    if last_print_dot:
                        print()
                    print(
                        (
                            f"   f: {int(time_taken)}s: {human_readable_size(file_size)}: "
                            f"{str(entry)}"
                        )
                    )
                else:
                    print(".", end="", flush=True)
                    last_print_dot = True
            elif entry.is_dir():
                # Recursively process subdirectories first (leaf-first)
                last_print_dot = process_directory(
                    entry, file_info, dir_info, csv_writer, last_print_dot
                )

                # Lookup subdirectory's size and CRC and add to the current directory's totals
                subdir_size, _, subdir_crc = dir_info[entry]
                dir_size += subdir_size
                dir_crc = zlib.crc32(subdir_crc.encode("utf-8"), dir_crc)
        except OSError as err:  # includes PermissionError, FileNotFoundError
            if last_print_dot:
                print()
            # show errors, but ignore them
            print(str(err), file=sys.stderr)
            # Do nothing else for OSErrors, for now.
        except KeyboardInterrupt:
            if last_print_dot:
                print()
            print("Killed: shutting down...", file=sys.stderr)
            sys.exit(2)
        except Exception as err:  # pylint: disable=broad-exception-caught
            if last_print_dot:
                print()
            # show errors, but ignore them
            print("+" * 88, file=sys.stderr)
            print(str(err), file=sys.stderr)
            # Do nothing else for all other errors, for now.

    time_taken_dir = time.time() - start_time_dir
    if time_taken_dir > 1:
        if last_print_dot:
            print()
        print(
            f"   d: {int(time_taken_dir)}s: {human_readable_size(dir_size)}: {str(directory)}"
        )
    else:
        print(".", end="", flush=True)
        last_print_dot = True

    # Store directory's size and CRC in the dir_info lookup
    dir_info[directory] = (dir_size, time_taken_dir, f"{dir_crc & 0xFFFFFFFF:08x}")

    # Write current directory's data to the CSV incrementally
    write_partial_csv(csv_writer, file_info, dir_info)

    # Clear memory for this directory
    file_info.clear()
    dir_info.clear()

    return last_print_dot


def write_partial_csv(
    csv_writer: csv.writer,
    file_info: Dict[Path, Tuple[int, float, str]],
    dir_info: Dict[Path, Tuple[int, float, str]],
) -> None:
    """Write partial manifest information to the CSV file.

    Each entry includes: path, modified time, size, and CRC.
    """
    # Write file info
    for path, (size, time_taken, crc) in file_info.items():
        csv_writer.writerow([str(path), get_modified_time(path), size, time_taken, crc])

    # Write directory info
    for path, (size, time_taken, crc) in dir_info.items():
        csv_writer.writerow([str(path), get_modified_time(path), size, time_taken, crc])


def generate_manifest(root_dir: Path, output_csv: Path) -> None:
    """Generate a manifest of all files and directories under the root_dir.

    Outputs the result to output_csv in CSV format.
    """
    file_info: Dict[Path, Tuple[int, float, str]] = (
        {}
    )  # Store file metadata: {file_path: (size, crc)}
    dir_info: Dict[Path, Tuple[int, float, str]] = (
        {}
    )  # Store directory metadata: {dir_path: (size, crc)}

    with open(output_csv, mode="w", newline="", encoding="utf-8") as csv_file:
        writer = csv.writer(csv_file)
        writer.writerow(
            ["Path", "Modified Time", "Size (bytes)", "Time taken (seconds)", "CRC32"]
        )

        # Start recursive processing from the root directory
        last_print_dot = process_directory(root_dir, file_info, dir_info, writer, False)
        if last_print_dot:
            print()

    print(f"scan complete, CSV '{output_csv}' written incrementally.")


def parse_arguments() -> argparse.Namespace:
    """Parse command-line arguments using argparse.

    Returns the parsed arguments.
    """
    parser = argparse.ArgumentParser(
        description="Generate a manifest of all files and directories in a disk directory."
    )
    parser.add_argument("root_directory", type=Path, help="Root directory to scan")
    parser.add_argument("output_csv", type=Path, help="Output CSV file path")
    return parser.parse_args()


def main() -> None:
    """Main entry point."""
    # Parse arguments
    args = parse_arguments()

    # Generate manifest using the provided root directory and output file
    generate_manifest(args.root_directory, args.output_csv)


if __name__ == "__main__":
    main()
