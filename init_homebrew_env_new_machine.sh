# command line utils I use a lot
brew install fd tig ag git-gui 

# vim and nvim utils and plugin deps
brew install nvim fontconfig vim

# development tools and deps (most web/app specific)
brew install npm node cocoapods rbenv ruby-build jpeg java 

# output some useful information on ruby (used by fastlane amoungst other dev tools)
rbenv install -l
rbenv init

# Support localy-managed and shared obsidian instance:
# - Couchdb for remote obsidian.
# - cloudflared for allowing https access over the net to the local instance.
# - tcl-tk for tkinter support
brew install couchdb cloudflared tcl-tk
brew services start couchdb
