"""Runem hooks for testing conformance of python files."""

import os
import pathlib
import typing

from pkg_resources import DistributionNotFound, VersionConflict, require
from runem.run_command import run_command
from runem.types import FilePathList, JobName, Options


def check_requirements_file(requirements_path: pathlib.Path) -> typing.Set[str]:
    """Checks if the dependencies specified in a requirements.txt file are met in the
    current environment.

    Parameters:
    - requirements_path (str): Path to the requirements.txt file.

    Returns:
    - List[str]: A list of unmet dependencies. Empty if all dependencies are met.
    """
    unmet_dependencies = set()  # Set to store names of unmet dependencies

    # Ensure the requirements file exists
    if not requirements_path.exists():
        raise FileNotFoundError(f"{requirements_path} does not exist.")

    # Open and read the requirements file
    with requirements_path.open("r") as file:
        for line in file:
            line = line.strip()
            # Skip empty lines and comments
            if not line or line.startswith("#"):
                continue
            try:
                # Check if the dependency is met
                require(line)
            except (DistributionNotFound, VersionConflict) as e:
                # If a dependency is not met, add it to the list
                unmet_dependencies.add(str(e))

    return unmet_dependencies


def check_multiple_requirements_files(
    file_paths: typing.List[pathlib.Path],
) -> typing.Set[str]:
    """Call check_requirements_file for multiple file paths, aggregating results into a
    single set.

    Args:
        file_paths: A list of Path objects representing the file paths to check.

    Returns:
        A set of unmet dependencies across all specified files.
    """
    unmet_dependencies: typing.Set[str] = set()
    for file_path in file_paths:
        unmet_dependencies.update(check_requirements_file(file_path))
    return unmet_dependencies


def _job_py_pre_check(**kwargs: typing.Any) -> None:
    """Ensures we are in the right state for the python utilities to be used."""
    if "VIRTUAL_ENV" not in os.environ:
        raise RuntimeError("Not in a virtual environment")

    root_path: pathlib.Path = kwargs["root_path"]

    requirements_files = list(root_path.glob("requirements*.txt"))
    assert requirements_files, f"no requirements files in '{root_path}'"
    unmet_dependencies: typing.Set[str] = check_multiple_requirements_files(
        requirements_files
    )
    if unmet_dependencies:
        raise RuntimeError(f"missing python deps {unmet_dependencies}")


def _job_py_code_reformat(
    **kwargs: typing.Any,
) -> None:
    """Runs python formatting code in serial order as one influences the other."""
    label: JobName = kwargs["label"]
    options: Options = kwargs["options"]
    python_files: FilePathList = kwargs["file_list"]

    # put into 'check' mode if requested on the command line
    extra_args = []
    docformatter_extra_args = [
        "--in-place",
    ]
    if options["check_only"]:
        extra_args.append("--check")
        docformatter_extra_args = []  # --inplace is not compatible with --check

    if options["isort"]:
        isort_cmd = [
            "python3",
            "-m",
            "isort",
            "--profile",
            "black",
            "--treat-all-comment-as-code",
            *extra_args,
            *python_files,
        ]
        kwargs["label"] = f"{label} isort"
        run_command(cmd=isort_cmd, **kwargs)

    if options["black"]:
        black_cmd = [
            "python3",
            "-m",
            "black",
            *extra_args,
            *python_files,
        ]
        kwargs["label"] = f"{label} black"
        run_command(cmd=black_cmd, **kwargs)

    if options["docformatter"]:
        docformatter_cmd = [
            "python3",
            "-m",
            "docformatter",
            "--wrap-summaries",
            "88",
            "--wrap-descriptions",
            "88",
            *docformatter_extra_args,
            *extra_args,
            *python_files,
        ]
        allowed_exits: typing.Tuple[int, ...] = (
            0,  # no work/change required
            3,  # no errors, but code was reformatted
        )
        if options["check_only"]:
            # in check it is ONLY ok if no work/change was required
            allowed_exits = (0,)
        kwargs["label"] = f"{label} docformatter"
        run_command(
            cmd=docformatter_cmd,
            ignore_fails=False,
            valid_exit_ids=allowed_exits,
            **kwargs,
        )


def _job_py_pylint(
    **kwargs: typing.Any,
) -> None:
    python_files: FilePathList = kwargs["file_list"]
    root_path: pathlib.Path = kwargs["root_path"]

    pylint_cfg = root_path / ".pylint.rc"
    if not pylint_cfg.exists():
        raise RuntimeError(f"PYLINT Config not found at '{pylint_cfg}'")

    fixme_list: typing.List[str] = [
        "bin/_to_sort/includeAsDotGraph.py",
        "bin/_to_sort/parseIncludesList.py",
        # "bin/git/git-booboos.py",
        "bin/git/git-code-review/git-reviewboard-post.py",
        "bin/git/git-code-review/git-reviewboard-rbt.py",
        "bin/git/git-rebase-all-branches.py",
        "bin/git/git-sort-commits.py",
        "bin/update_large_files.py",
    ]

    files_to_lint: typing.List[str] = list(set(python_files) - set(fixme_list))

    pylint_cmd = [
        "python3",
        "-m",
        "pylint",
        "-j1",
        "--score=n",
        f"--rcfile={pylint_cfg}",
        *files_to_lint,
    ]
    run_command(cmd=pylint_cmd, **kwargs)


def _job_py_flake8(
    **kwargs: typing.Any,
) -> None:
    python_files: FilePathList = kwargs["file_list"]
    root_path: pathlib.Path = kwargs["root_path"]
    flake8_rc = root_path / ".flake8"
    if not flake8_rc.exists():
        raise RuntimeError(f"flake8 config not found at '{flake8_rc}'")

    flake8_cmd = [
        "python3",
        "-m",
        "flake8",
        *python_files,
    ]
    run_command(cmd=flake8_cmd, **kwargs)


def _job_py_mypy(
    **kwargs: typing.Any,
) -> None:
    python_files: FilePathList = kwargs["file_list"]
    mypy_cmd = ["python3", "-m", "mypy", *python_files]
    run_command(cmd=mypy_cmd, **kwargs)


def _install_python_requirements(
    **kwargs: typing.Any,
) -> None:
    options: Options = kwargs["options"]
    root_path: pathlib.Path = kwargs["root_path"]

    global_deps_install_requested: bool = options["deps-install"]
    if not global_deps_install_requested:
        python_install_deps_requested: bool = options["deps-install-python"]
        if not python_install_deps_requested:
            # not enabled
            return

    if "VIRTUAL_ENV" not in os.environ:
        raise RuntimeError(
            "'dep-install' or 'deps-install-python requested' but NOT in a virtual environment!"
        )

    requirements_path = root_path
    requirements_files = list(requirements_path.glob("requirements*.txt"))
    if not requirements_files:
        raise RuntimeError(
            f"lursight: requirements files not found at {str(requirements_path.absolute())}"
        )
    cmd = [
        "python3",
        "-m",
        "pip",
        "install",
        "--upgrade",
    ]
    for req_file in requirements_files:
        cmd.extend(["--requirement", str(req_file)])
    run_command(cmd=cmd, **kwargs)
