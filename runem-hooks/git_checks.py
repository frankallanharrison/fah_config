"""Check the condition of the git repo."""

import pathlib
import subprocess
import typing

from runem.types import Options


def _check_for_old_package_dirs(
    **kwargs: typing.Any,
) -> None:
    """Capture the fact that the packages dirs for the app have been renamed.

    They were renamed to make faster to type (I was typing them all the time).
    This help sport issues with dud data existing before it becomes a
    problem.

    TODO: this is a short-lived check added 04/2024.
    """
    root_path: pathlib.Path = kwargs["root_path"]
    packages_path = root_path / "app" / "packages"
    if not packages_path.exists():
        raise RuntimeError(
            f"can't find the packages dir, looked in '{str(packages_path.absolute())}'"
        )

    for deprecated in ("app_lang", "app_lang_web", "app_chat"):
        deprecated_path = packages_path / deprecated
        if deprecated_path.exists():
            replaced = deprecated.replace("_", "-")
            replaced_path = packages_path / replaced
            assert replaced_path.exists()
            raise RuntimeError(
                f"Deprecated path found! '{deprecated}'. Move any pending work to "
                f"'{replaced}' and delete the path.\n"
                f"'{deprecated_path.absolute()}' -> '{replaced_path.absolute()}'"
            )


def _ensure_no_git_diffs(
    **kwargs: typing.Any,
) -> None:
    """Ensure we do NOT have any dangling changes.

    This is designed to be used on pre-push to ensure that we didn't
    accidentally miss things like:
    - format changes (which *should* be caught by the `--check` option).
    - Podfile.lock changes (which might have been missed because upstream
      cocoa-pod deps changed, or a new npm packages was added).
    """
    options: Options = kwargs["options"]
    if not options["git-no-diffs"]:
        # for normal running it is VERY likely a dev will have diffs locally, so
        # don't run the diff check.
        return

    # Run git diff and capture output
    result = subprocess.run(
        ["git", "diff", "--stat"],
        capture_output=True,
        text=True,
        check=False,  # I'm torn on this, but for now do NOT error if non-zero
    )

    # Check if there are any diffs
    if result.stdout.strip():
        # Error if there are
        raise RuntimeError(f"There are uncommitted changes.\n{result}")
